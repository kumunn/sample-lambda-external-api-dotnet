using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.Lambda.Core; 
using System.Net.Http;
using Amazon.KeyManagementService;
using Amazon.KeyManagementService.Model;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace MyProject
{
    public class Function
    {
        
        public string FunctionHandler(string input, ILambdaContext context)
        {
            var username = Environment.GetEnvironmentVariable("username");
            var endpoint = Environment.GetEnvironmentVariable("endpoint");
            var tenantCode = Environment.GetEnvironmentVariable("tenantCode");
            
            var parameter = new Dictionary<string, string>();
            parameter.Add("grant_type", "password");
            parameter.Add("username", username);
            var decryptedPass = DecodeEnvVar("password").Result;

            parameter.Add("password", decryptedPass);
            parameter.Add("scope", "cn mail sn givenname uid employeeNumber");

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = 
							new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", "IGRhdGFodWJfRDFSMTkxOkF0QDZMOGJ3ZjI=");
            var content = new FormUrlEncodedContent(parameter);
            var result = client.PostAsync(endpoint, content).Result;   

            var res = result.Content.ReadAsStringAsync().Result;
			JObject json = JObject.Parse(res);
			var token = json.GetValue("access_token");
            LambdaLogger.Log("Respose code of api to get access token is ::: " + result.StatusCode);
            
            HttpClient client1 = new HttpClient();
            client1.DefaultRequestHeaders.Authorization = 
                        new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.ToString());
            client1.DefaultRequestHeaders.Add("X-XREF-HEADER","valid");
            client1.DefaultRequestHeaders.Add("X-ADH-Tenant-Code", tenantCode);
            //var content = new FormUrlEncodedContent(paramss);
            var result1 = client1.GetAsync("https://u-acpapi.datahub.epsilon.com/catalog/api/atlas/v2/search/dsl?typeName=rdbms_instance&query=name%3D\"dcrm\" and __state%3D\"ACTIVE\"").Result;   

            var res1 = result1.Content.ReadAsStringAsync().Result;
            LambdaLogger.Log("\n Respose code of api to entity search for query ::: " + result1.StatusCode);
            LambdaLogger.Log("\n Final Response of Atlas search api:::: " + res1);
            
            return res1;
        }

        private static async Task<string> DecodeEnvVar(string envVarName)
        {
            // Retrieve env var text
            var encryptedBase64Text = Environment.GetEnvironmentVariable(envVarName);
            // Convert base64-encoded text to bytes
            var encryptedBytes = Convert.FromBase64String(encryptedBase64Text);
            // Construct client
            using (var client = new AmazonKeyManagementServiceClient())
            {
                // Construct request
                var decryptRequest = new DecryptRequest
                {
                    CiphertextBlob = new MemoryStream(encryptedBytes),
                };
                // Call KMS to decrypt data
                var response = await client.DecryptAsync(decryptRequest);
                using (var plaintextStream = response.Plaintext)
                {
                    // Get decrypted bytes
                    var plaintextBytes = plaintextStream.ToArray();
                    // Convert decrypted bytes to ASCII text
                    var plaintext = Encoding.UTF8.GetString(plaintextBytes);
                    return plaintext;
                }
            }
        }
    }
}
